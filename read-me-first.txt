Hi,

I have completed the test and pushed the code on a public git repository.

https://gitlab.com/amir-arain/laraveltest.git

kindly follow below steps.

1. composer install
2. php artisan config:clear
3. create a database of your own
4. duplicate a .env.example file to .env and put database name in .env file and correct APP_URL according to folder location all other settings are already set in the file like mailtrap or application key.
5. php artisan config:clear
6. php artisan migrate
7. php artisan db:seed
7. php artisan storage:link

admin user email and password is

email : admin@admin.com
password : password

please run commands below in project root via command line once before start checking the project

php artisan config:clear
php artisan cache:clear
php artisan view:clear
php artisan route:clear
php artisan clear-compiled
composer dumpautoload
php artisan serve


mailtrap login

email => mytestinga7@gmail.com
password => mytestinga7

if in any condition project don't work accordingly then you can call me to come there and give a demo of this work.

Kindly acknowledge of safe receive of this email.